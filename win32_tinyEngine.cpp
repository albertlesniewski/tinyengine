#include <Windows.h>
#include <stdint.h>
#include <Xinput.h>
#include <dsound.h>
#include <math.h>

#define local_presist static 
#define global_variable static
#define internal static

typedef int32_t bool32;
typedef float real32;
typedef double real64;


#define X_INPUT_GET_STATE(name) DWORD WINAPI name(DWORD dwUserIndex, XINPUT_STATE* pState)
typedef X_INPUT_GET_STATE(x_input_get_state);
X_INPUT_GET_STATE(XInputGetStateStub) {
	return ERROR_DEVICE_NOT_CONNECTED;
}
global_variable x_input_get_state* XInputGetState_ = XInputGetStateStub;
#define XInputGetState XInputGetState_ 

#define X_INPUT_SET_STATE(name) DWORD WINAPI name(DWORD dwUserIndex, XINPUT_VIBRATION* pVibration)
typedef X_INPUT_SET_STATE(x_input_set_state);
X_INPUT_SET_STATE(XInputSetStateStub) {
	return ERROR_DEVICE_NOT_CONNECTED;
}
global_variable x_input_set_state* XInputSetState_ = XInputSetStateStub;
#define XInputSetState XInputSetState_

#define DIRECT_SOUND_CREATE(name) HRESULT WINAPI name(LPCGUID pcGuidDevice, LPDIRECTSOUND* ppDS, LPUNKNOWN pUnkOuter);
typedef DIRECT_SOUND_CREATE(direct_sound_create);



struct win32_OffscreenBuffer
{
	BITMAPINFO Info = {};
	void* Memory;
	int Width;
	int Height;
	int Pitch;
};

struct win32_ClientDimensions
{
	int Width;
	int Height;
};

global_variable bool Running;
global_variable win32_OffscreenBuffer GlobalBackBuffer;
global_variable LPDIRECTSOUNDBUFFER GlobalSecondaryBuffer;

internal LRESULT CALLBACK win32MainWindowCallback(HWND Window, UINT Message, WPARAM WParam, LPARAM LParam);
internal void win32ResizeDIBSection(win32_OffscreenBuffer* OffscreenBuffer, int ClientWidth, int ClientHeight);
internal void win32DisplayBufferToWindow(HDC DeviceContext, int ClientWidth, int ClientHeight, win32_OffscreenBuffer* OffscreenBuffer);
internal void RenderGradient(win32_OffscreenBuffer* OffscreenBuffer, int XOffset, int YOffset);
internal win32_ClientDimensions win32GetClientDimension(HWND WindowHandle);
internal void win32LoadXInput();
internal void win32InitDSound(HWND Window, int32_t BufferSize, int32_t SamplesPerSecond);


int CALLBACK WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd) {

	WNDCLASSEXA WindowClass = {};

	WindowClass.cbSize = sizeof(WNDCLASSEXA);
	WindowClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	WindowClass.lpfnWndProc = win32MainWindowCallback;
	WindowClass.hInstance = hInstance;
	//WindowClass.hIcon = ;
	WindowClass.lpszClassName = "TinyEngineWindowClass";

	win32LoadXInput();

	win32ResizeDIBSection(&GlobalBackBuffer, 1280, 720);

	if (RegisterClassExA(&WindowClass)) {
		HWND WindowHandle = CreateWindowExA(0, WindowClass.lpszClassName, "TinyEngine", WS_OVERLAPPEDWINDOW|WS_VISIBLE, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, 0, 0, hInstance, 0);
		if (WindowHandle) {
			HDC DeviceContext = GetDC(WindowHandle);
			int XOffset = 0;
			int YOffset = 0;

			int SamplesPerSecond = 48000;
			int BytesPerSample = sizeof(int16_t) * 2;
			int ToneHz = 256;
			uint16_t ToneVolume = 100;
			uint32_t RunningSampleIndex = 0;
			int WavePeriod = SamplesPerSecond / ToneHz;
			int SecondaryBufferSize = BytesPerSample * SamplesPerSecond;
			Running = true;

			win32InitDSound(WindowHandle, SecondaryBufferSize, SamplesPerSecond);
			GlobalSecondaryBuffer->Play(0, 0, DSBPLAY_LOOPING);

			while(Running) {
				MSG Message;
				while(PeekMessageA(&Message, 0, 0, 0, PM_REMOVE)) {
					if (Message.message == WM_QUIT) {
						Running = false;
					}
					TranslateMessage(&Message);
					DispatchMessageA(&Message);
				}
				 // TODO: Shouled we pull controller states more frequently?
				for (int ControllerIndex = 0; ControllerIndex < XUSER_MAX_COUNT; ControllerIndex++) {
					XINPUT_STATE ControllerState;
					if (XInputGetState(ControllerIndex, &ControllerState) == ERROR_SUCCESS) {
						XINPUT_GAMEPAD* Pad = &ControllerState.Gamepad;

						bool Up = (Pad->wButtons & XINPUT_GAMEPAD_DPAD_UP);
						bool Down = (Pad->wButtons & XINPUT_GAMEPAD_DPAD_DOWN);
						bool Left = (Pad->wButtons & XINPUT_GAMEPAD_DPAD_LEFT);
						bool Right = (Pad->wButtons & XINPUT_GAMEPAD_DPAD_RIGHT);
						bool Start = (Pad->wButtons & XINPUT_GAMEPAD_START);
						bool Back = (Pad->wButtons & XINPUT_GAMEPAD_BACK);
						bool LeftShoulder = (Pad->wButtons & XINPUT_GAMEPAD_LEFT_SHOULDER);
						bool RightShoulder = (Pad->wButtons & XINPUT_GAMEPAD_RIGHT_SHOULDER);
						bool AButton = (Pad->wButtons & XINPUT_GAMEPAD_A);
						bool BButton = (Pad->wButtons & XINPUT_GAMEPAD_B);
						bool XButton = (Pad->wButtons & XINPUT_GAMEPAD_X);
						bool YButton = (Pad->wButtons & XINPUT_GAMEPAD_Y);

						int16_t StickX = Pad->sThumbLX;
						int16_t StickY = Pad->sThumbLY;

						XOffset += (StickX >> 12);
						YOffset += (StickY >> 12);
					}
					else {
						//TODO: Controller not plugged / Error?
					}
				}
				XINPUT_VIBRATION Vibration;
				Vibration.wLeftMotorSpeed = 60000;
				Vibration.wRightMotorSpeed = 60000;
				//XInputSetState(0, &Vibration);

				RenderGradient(&GlobalBackBuffer, XOffset, YOffset);

				DWORD PlayCursor;
				DWORD WriteCursor;

				if (SUCCEEDED(GlobalSecondaryBuffer->GetCurrentPosition(&PlayCursor, &WriteCursor))) {

					DWORD ByteToLock = RunningSampleIndex * BytesPerSample % SecondaryBufferSize;
					DWORD BytesToWrite;
					if (ByteToLock == PlayCursor) {
						BytesToWrite = SecondaryBufferSize;
					}
					else if (ByteToLock > PlayCursor) {
						BytesToWrite = (SecondaryBufferSize - ByteToLock);
						BytesToWrite += PlayCursor;
					}
					else {
						BytesToWrite = PlayCursor - ByteToLock;
					}
					VOID* Region1;
					DWORD Region1Size;
					VOID* Region2;
					DWORD Region2Size;

					if (SUCCEEDED(GlobalSecondaryBuffer->Lock(ByteToLock, BytesToWrite, &Region1, &Region1Size, &Region2, &Region2Size, 0))) {
						// int16 int16  int16 int16  int16 int16 ...
						//[LEFT RIGHT] [LEFT RIGHT] [LEFT RIGHT] [LEFT RIGHT] [LEFT RIGHT] ....
						//
						//Buffer contains samples, we have 2 channels (setereo), so we need to values (int16) for 1 sample, buffer needs to be on double int16 boundries (32 bit)
						//TODO: Assert that Region1Size/Region2Size is valid
						int16_t* SampleOut = (int16_t*)Region1;
						DWORD Region1SampleCount = Region1Size / BytesPerSample;
						for (DWORD SampleIndex = 0; SampleIndex < Region1SampleCount; ++SampleIndex) {
							int16_t SampleValue = ((RunningSampleIndex++ / (WavePeriod / 2)) % 2) ? ToneVolume : -ToneVolume;
							*SampleOut++ = SampleValue;
							*SampleOut++ = SampleValue;
						}

						SampleOut = (int16_t*)Region2;
						DWORD Region2SampleCount = Region2Size / BytesPerSample;
						for (DWORD SampleIndex = 0; SampleIndex < Region2SampleCount; ++SampleIndex) {
							int16_t SampleValue = ((RunningSampleIndex++ / (WavePeriod / 2)) % 2) ? ToneVolume : -ToneVolume;
							*SampleOut++ = SampleValue;
							*SampleOut++ = SampleValue;
						}
						GlobalSecondaryBuffer->Unlock(Region1, Region1Size, Region2, Region2Size);
					}
					else {
						//TODO: Logging 
					}
				}
				else {
					//TODO: Logging
				}

				win32_ClientDimensions ClientDim = win32GetClientDimension(WindowHandle);
				win32DisplayBufferToWindow(DeviceContext, ClientDim.Width, ClientDim.Height, &GlobalBackBuffer);
			}
		}
		else {
			// TODO: LOGGING
		}
	}
	else {
		// TODO: LOGGING
	}

	return 0;
}

internal LRESULT CALLBACK win32MainWindowCallback(HWND Window, UINT Message, WPARAM WParam, LPARAM LParam) {

	LRESULT Result = 0;

	switch (Message) {
		case WM_DESTROY:
		{
			//TODO: Handle as error?
			Running = false;
			OutputDebugStringA("WM_DESTROY\n");
		}break;

		case WM_CLOSE:
		{
			Running = false;
			OutputDebugStringA("WM_CLOSE\n");
		}break;

		case WM_ACTIVATEAPP:
		{
			OutputDebugStringA("WM_ACTIVATEAPP\n");
		}break;

		case WM_PAINT:
		{
			PAINTSTRUCT Paint;
			HDC DeviceContext = BeginPaint(Window, &Paint);

			win32_ClientDimensions ClientDim = win32GetClientDimension(Window);

			win32DisplayBufferToWindow(DeviceContext, ClientDim.Width, ClientDim.Height, &GlobalBackBuffer);
			EndPaint(Window, &Paint);

			OutputDebugStringA("WM_PAINT\n");
		}break;

		case WM_SYSKEYDOWN:
		case WM_SYSKEYUP:
		case WM_KEYDOWN:
		case WM_KEYUP:
		{
			uint32_t VKCode = WParam;
			bool WasDown = ((LParam & (1 << 30)) != 0);
			bool IsDown = ((LParam & (1 << 31)) == 0);

			if (WasDown != IsDown) {
				if (VKCode == 'W') {

				}
				else if (VKCode == 'A') {

				}
				else if (VKCode == 'S') {

				}
				else if (VKCode == 'D') {

				}
				else if (VKCode == 'Q') {

				}
				else if (VKCode == 'E') {

				}
				else if (VKCode == VK_UP) {

				}
				else if (VKCode == VK_DOWN) {

				}
				else if (VKCode == VK_LEFT) {

				}
				else if (VKCode == VK_RIGHT) {

				}
				else if (VKCode == VK_ESCAPE) {
					OutputDebugStringA("ESCAPE: ");
					if (WasDown) {
						OutputDebugStringA("WASDOWN");
					}
					if (IsDown) {
						OutputDebugStringA("ISDOWN");
					}
					OutputDebugStringA("\n");
				}
				else if (VKCode == VK_SPACE) {

				}
				bool32 AltKeyWasDown = ((LParam & (1 << 29)) != 0);
				if ((VKCode == VK_F4) && AltKeyWasDown) {
					Running = false;
				}
			}
		}

		default:
		{
			//OutputDebugStringA("DEFAULT\n");
			Result = DefWindowProcA(Window, Message, WParam, LParam);
		}break;
	}

	return Result;
}

internal void win32ResizeDIBSection(win32_OffscreenBuffer* OffscreenBuffer, int ClientWidth, int ClientHeight) {

	if (OffscreenBuffer->Memory) {
		VirtualFree(OffscreenBuffer->Memory, 0, MEM_RELEASE);
	}

	OffscreenBuffer->Width = ClientWidth;
	OffscreenBuffer->Height = ClientHeight;
	int BytesPerPixel = 4;

	OffscreenBuffer->Info.bmiHeader.biSize = sizeof(OffscreenBuffer->Info.bmiHeader);
	OffscreenBuffer->Info.bmiHeader.biWidth = OffscreenBuffer->Width;
	OffscreenBuffer->Info.bmiHeader.biHeight = -OffscreenBuffer->Height;
	OffscreenBuffer->Info.bmiHeader.biPlanes = 1;
	OffscreenBuffer->Info.bmiHeader.biBitCount = 32;
	OffscreenBuffer->Info.bmiHeader.biCompression = BI_RGB;

	int BitmapMemorySize = (OffscreenBuffer->Width * OffscreenBuffer->Height) * BytesPerPixel;

	OffscreenBuffer->Memory = VirtualAlloc(0, BitmapMemorySize, MEM_RESERVE|MEM_COMMIT, PAGE_READWRITE);

	OffscreenBuffer->Pitch = ClientWidth * BytesPerPixel;

}

internal void win32DisplayBufferToWindow(HDC DeviceContext, int ClientWidth, int ClientHeight, win32_OffscreenBuffer* OffscreenBuffer) {

	StretchDIBits(DeviceContext, 0, 0, ClientWidth, ClientHeight, 0, 0, OffscreenBuffer->Width, OffscreenBuffer->Height, OffscreenBuffer->Memory, &OffscreenBuffer->Info, DIB_RGB_COLORS, SRCCOPY);
}

internal void RenderGradient(win32_OffscreenBuffer* OffscreenBuffer,int XOffset, int YOffset) {

	uint8_t* Row = (uint8_t*)OffscreenBuffer->Memory;
	for (int Y = 0; Y < OffscreenBuffer->Height; ++Y) {
		uint32_t* Pixel = (uint32_t*)Row;
		for (int X = 0; X < OffscreenBuffer->Width; ++X) {

			uint8_t Blue = (X + XOffset);
			uint8_t Green = (Y + YOffset);

			*Pixel++ = ((Green << 8) | Blue);
		}
		Row += OffscreenBuffer->Pitch;
	}
}

internal win32_ClientDimensions win32GetClientDimension(HWND WindowHandle) {
	RECT ClientRect;
	win32_ClientDimensions Dim;
	GetClientRect(WindowHandle, &ClientRect);
	Dim.Width = ClientRect.right - ClientRect.left;
	Dim.Height = ClientRect.bottom - ClientRect.top;
	return Dim;
}

internal void win32LoadXInput() {
	HMODULE XInputLibrary = LoadLibraryA("xinput1_4.dll");
	if (!XInputLibrary) {
		XInputLibrary = LoadLibraryA("xinput1_3.dll");
	}

	if (XInputLibrary) {
		XInputGetState = (x_input_get_state*)GetProcAddress(XInputLibrary, "XInputGetState");
		if (!XInputGetState) {
			XInputGetState = XInputGetStateStub;
		}

		XInputSetState = (x_input_set_state*)GetProcAddress(XInputLibrary, "XInputSetState");
		if (!XInputSetState) {
			XInputSetState = XInputSetStateStub;
		}
	}
}

internal void win32InitDSound(HWND Window, int32_t BufferSize, int32_t SamplesPerSecond) {
	HMODULE DirectSoundLibrary = LoadLibraryA("dsound.dll");

	if (DirectSoundLibrary) {
		direct_sound_create* DirectSoundCreate = (direct_sound_create*)GetProcAddress(DirectSoundLibrary, "DirectSoundCreate");
		LPDIRECTSOUND DirectSound;
		if (DirectSoundCreate && SUCCEEDED(DirectSoundCreate(0, &DirectSound, 0))) {

			WAVEFORMATEX WaveFormat = {};
			WaveFormat.wFormatTag = WAVE_FORMAT_PCM;
			WaveFormat.nChannels = 2;
			WaveFormat.nSamplesPerSec = SamplesPerSecond;
			WaveFormat.wBitsPerSample = 16;
			WaveFormat.nBlockAlign = (WaveFormat.nChannels * WaveFormat.wBitsPerSample) / 8;
			WaveFormat.nAvgBytesPerSec = WaveFormat.nSamplesPerSec * WaveFormat.nBlockAlign;

			if (SUCCEEDED(DirectSound->SetCooperativeLevel(Window, DSSCL_PRIORITY))) {

				DSBUFFERDESC PrimaryBufferDescription = {};
				PrimaryBufferDescription.dwSize = sizeof(PrimaryBufferDescription);
				PrimaryBufferDescription.dwFlags = DSBCAPS_PRIMARYBUFFER;
				
				LPDIRECTSOUNDBUFFER PrimaryBuffer;

				if (SUCCEEDED(DirectSound->CreateSoundBuffer(&PrimaryBufferDescription, &PrimaryBuffer, 0))) {
					if (SUCCEEDED(PrimaryBuffer->SetFormat(&WaveFormat))) {
						OutputDebugStringA("Primary Buffer is initialized and format is set!\n");
					}
					else {
						//TODO: Logging
					}
				}
				else {
					//TODO: Logging
				}
			}
			else {
				//TODO: LOGGING
			}
			DSBUFFERDESC SecondaryBufferDescription = {};
			SecondaryBufferDescription.dwSize = sizeof(SecondaryBufferDescription);
			SecondaryBufferDescription.dwBufferBytes = BufferSize;
			SecondaryBufferDescription.lpwfxFormat = &WaveFormat;

			if (SUCCEEDED(DirectSound->CreateSoundBuffer(&SecondaryBufferDescription, &GlobalSecondaryBuffer, 0))) {
				OutputDebugStringA("Secondary Buffer is initialized!\n");
			}
			else {
				//TODO: Logging
			}
		}
		else {
			//TODO: Logging
		}
	}
	else {
		//TODO: Logging
	}
}